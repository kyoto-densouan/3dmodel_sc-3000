# README #

1/3スケールのSEGA SC-3000風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。

***

# 実機情報

## メーカ
- SEGA

## 発売時期
- 1983年7月15日

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/SC-3000)
- [セガハード大百科](https://sega.jp/history/hard/sc3000/index.html)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_sc-3000/raw/e45bade5e2fba344a4d08b4831fac3c161e30819/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_sc-3000/raw/e45bade5e2fba344a4d08b4831fac3c161e30819/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_sc-3000/raw/e45bade5e2fba344a4d08b4831fac3c161e30819/ExampleImage.png)
